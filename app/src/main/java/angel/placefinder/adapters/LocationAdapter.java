package angel.placefinder.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import angel.placefinder.R;
import angel.placefinder.activities.MainActivity;
import angel.placefinder.models.Location;

public class LocationAdapter extends BaseAdapter {
    private List<Location> locations;
    private int layout;
    private Context context;
    private MainActivity act;
    public LocationAdapter(List<Location> locations, int layout, Context context,MainActivity act) {
        this.locations = locations;
        this.layout = layout;
        this.context = context;
        this.act = act;
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int i) {
        return locations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layout,null);
            vh = new ViewHolder();
            vh.btnGo = convertView.findViewById(R.id.adapterbtnGo);
            vh.btnRemove = convertView.findViewById(R.id.adapterbtnRemove);
            vh.btnRoute = convertView.findViewById(R.id.adapterbtnRoute);
            vh.textName = convertView.findViewById(R.id.adapterTextName);
            vh.textDate = convertView.findViewById(R.id.adapterTextDate);
            convertView.setTag(vh);
        } else vh = (ViewHolder) convertView.getTag();

        final Location location = locations.get(i);
        vh.textDate.setText(location.getDatetime().toString());
        vh.textName.setText(location.getName());
        vh.btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchMap("google.navigation:q="+location.getLat()+","+location.getLon());
            }
        });

        vh.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                act.removeLocation(location);
            }
        });

        vh.btnRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchMap("geo:"+location.getLat()+","+location.getLon());
            }
        });

        return convertView;
    }

    private void launchMap(String uriString) {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mapIntent);
    }


    private class ViewHolder {
        private ImageButton btnGo;
        private ImageButton btnRemove;
        private ImageButton btnRoute;
        private TextView textName;
        private TextView textDate;
    }
}
