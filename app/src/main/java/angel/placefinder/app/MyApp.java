package angel.placefinder.app;

import android.app.Application;

import java.util.concurrent.atomic.AtomicInteger;

import angel.placefinder.models.Location;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class MyApp extends Application {
    public static AtomicInteger LocID= new AtomicInteger();

    @Override
    public void onCreate(){
        setUpRealmConfig();
        Realm realm = Realm.getDefaultInstance();
        LocID = getTableID(realm, Location.class);
        super.onCreate();
    }

    private void setUpRealmConfig() {
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }
    private <T extends RealmObject> AtomicInteger getTableID(Realm realm, Class<T> aClass) {
        RealmResults<T> results = realm.where(aClass).findAll();
        if (results.size() > 0) {
            return new AtomicInteger(results.max("id").intValue());
        } else return new AtomicInteger();
    }
}
