package angel.placefinder.models;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import angel.placefinder.app.MyApp;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Location extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private double lat;
    private double lon;
    private Date datetime;

    public Location(){}

    public Location(String name, double lat, double lon, Date datetime) {
        this.id = MyApp.LocID.incrementAndGet();
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.datetime = datetime;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
}
