package angel.placefinder.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.Calendar;

import angel.placefinder.R;
import angel.placefinder.adapters.LocationAdapter;
import angel.placefinder.models.Location;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener, RealmChangeListener<RealmResults<Location>>, AdapterView.OnItemClickListener {

    private EditText inputName;
    private ListView list;
    private FloatingActionButton fabAdd;
    private RealmResults<Location> locations;
    private LocationAdapter adapter;
    private LocationManager locManager;
    private double lat;
    private double lon;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 5, this);
        setGUI();
        locations = realm.where(Location.class).findAll();
        locations.addChangeListener(this);
        adapter = new LocationAdapter(locations, R.layout.location_adapter, getApplicationContext(), this);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
        fabAdd.setOnClickListener(this);
    }

    private void setGUI() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        inputName = findViewById(R.id.inputName);
        list = findViewById(R.id.list);
        fabAdd = findViewById(R.id.fab);
    }

    @Override
    public void onClick(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String name;
        if (inputName.getText().toString().isEmpty())
            name = lat + " x " + lon;
        else name = inputName.getText().toString();
        addLocation(new Location(name, lat, lon, Calendar.getInstance().getTime()));
        hideKeyboard(view);
        inputName.setText("");
        inputName.clearFocus();
        adapter.notifyDataSetChanged();
    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public void onChange(RealmResults<Location> locations) {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    public void addLocation(Location l) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(l);
        realm.commitTransaction();
        list.smoothScrollToPosition(locations.size() - 1);
    }

    public void removeLocation(Location l) {
        realm.beginTransaction();
        l.deleteFromRealm();
        realm.commitTransaction();
    }

    private void editBoard(String name, Location l) {
        realm.beginTransaction();
        l.setName(name);
        realm.copyToRealmOrUpdate(l);
        realm.commitTransaction();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        showAlertDialogEdit(i);
    }

    private void showAlertDialogEdit(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Editar nombre")
                .
    }
}
